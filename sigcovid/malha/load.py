# Popular tabelas espaciais do BDG apartir dos dados vetorial existente
import os
from django.contrib.gis.utils import LayerMapping

from .models import Municipio, RegiaoImediata, RegiaoIntermediaria, Estado

# Auto-generated `LayerMapping` dictionary for Muninipio model
municipio_mapping = {
    'cd_mun': 'CD_MUN',
    'nm_mun': 'NM_MUN',
    'pop2019': 'POP2019',
    'cep': 'CEP',
    'area_km2': 'AREA_KM2',
    'rgi':{'cd_rgi':'CD_RGI'},
    'rgint':{'cd_rgint':'CD_RGINT'},
    'uf':{'cd_uf':'CD_UF'},
#    'cd_rgi': 'CD_RGI',
#    'nm_rgi': 'NM_RGI',
#    'cd_rgint': 'CD_RGINT',
#    'nm_rgint': 'NM_RGINT',
#    'cd_uf': 'CD_UF',
#    'nm_uf': 'NM_UF',
#    'sigla_uf': 'SIGLA_UF',
#    'nm_regiao': 'NM_REGIAO',
#    'long_cent': 'LONG_CENT',
#    'lat_cent': 'LAT_CENT',
    'geom': 'MULTIPOLYGON',
}

rgimediata_mapping = {
    'cd_rgi': 'CD_RGI',
    'nm_rgi': 'NM_RGI',
    'pop2019': 'POP2019',
    'area_km2': 'AREA_KM2',
    'contem_mun': 'CONTEM_MUN',
    'rgint':{'cd_rgint':'CD_RGINT'},
    'uf':{'cd_uf':'CD_UF'},
#    'cd_rgint': 'CD_RGINT',
#    'nm_rgint': 'NM_RGINT',
#    'cd_uf': 'CD_UF',
#    'nm_uf': 'NM_UF',
#    'sigla_uf': 'SIGLA_UF',
#    'nm_regiao': 'NM_REGIAO',
#    'long_cent': 'LONG_CENT',
#    'lat_cent': 'LAT_CENT',
#    'geom': 'MULTIPOLYGON',
}

rgintermediaria_mapping = {
    'cd_rgint': 'CD_RGINT',
    'nm_rgint': 'NM_RGINT',
    'pop2019': 'POP2019',
    'area_km2': 'AREA_KM2',
    'contem_mun': 'CONTEM_MUN',
    'uf':{'cd_uf':'CD_UF'},
#    'cd_uf': 'CD_UF',
#    'nm_uf': 'NM_UF',
#    'sigla_uf': 'SIGLA_UF',
#    'nm_regiao': 'NM_REGIAO',
#    'long_cent': 'LONG_CENT',
#    'lat_cent': 'LAT_CENT',
#    'geom': 'MULTIPOLYGON',
}

estado_mapping = {
    'cd_uf': 'CD_UF',
    'nm_uf': 'NM_UF',
    'sigla_uf': 'SIGLA_UF',
    'pop2019': 'POP2019',
    'area_km2': 'AREA_KM2',
    'contem_mun': 'CONTEM_MUN',
    'nm_regiao': 'NM_REGIAO',
#    'long_cent': 'LONG_CENT',
#    'lat_cent': 'LAT_CENT',
    'geom': 'MULTIPOLYGON',
}

geojson_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Carrega camada de Municípios
def run_mun(verbose=True):
    geojson_data = os.path.abspath(os.path.join(geojson_dir, 'geojson/Piaui_Municipio_2020.geojson'))
    lm = LayerMapping(Municipio, geojson_data, municipio_mapping, source_srs=4674, encoding='utf-8')
    lm.save(strict=True, verbose=True)

# Carrega camada de Regiçoes Imediatas
def run_rgi(verbose=True):
    geojson_data = os.path.abspath(os.path.join(geojson_dir, 'geojson/Piaui_RGImediata_2020.geojson'))
    lm = LayerMapping(RegiaoImediata, geojson_data, rgimediata_mapping, source_srs=4674, encoding='utf-8')
    lm.save(strict=True, verbose=True)

# Carrega camada de Regiçoes Intermediarias
def run_rgint(verbose=True):
    geojson_data = os.path.abspath(os.path.join(geojson_dir, 'geojson/Piaui_RGIntermediaria_2020.geojson'))
    lm = LayerMapping(RegiaoIntermediaria, geojson_data, rgintermediaria_mapping, source_srs=4674, encoding='utf-8')
    lm.save(strict=True, verbose=True)

# Carrega camada de Estado
def run_uf(verbose=True):
    geojson_data = os.path.abspath(os.path.join(geojson_dir, 'geojson/Piaui_2020.geojson'))
    lm = LayerMapping(Estado, geojson_data, estado_mapping, source_srs=4674, encoding='utf-8')
    lm.save(strict=True, verbose=True)

