from django.db import models
from django.contrib.gis.db import models as gismodels

class Estado(gismodels.Model):
    cd_uf = gismodels.IntegerField(verbose_name='Código UF', primary_key=True, unique=True)
    nm_uf = gismodels.CharField(verbose_name='Nome Estado', max_length=100)
    sigla_uf = gismodels.CharField(verbose_name='Sigla UF', max_length=2)
    pop2019 = gismodels.IntegerField(verbose_name='População 2019')
    area_km2 = gismodels.DecimalField(verbose_name='Área (km²)', max_digits=20, decimal_places=4)
    contem_mun = gismodels.IntegerField(verbose_name='Qtd de Municípios contidos')
    nm_regiao = gismodels.CharField(verbose_name='Nome da Região', max_length=100)
    #long_cent = gismodels.DecimalField(verbose_name='Longitude do Centróide', max_digits=20, decimal_places=8)
    #lat_cent = gismodels.DecimalField(verbose_name='Latitude do Centróide', max_digits=20, decimal_places=8)
    geom = gismodels.MultiPolygonField(verbose_name='Geometria', srid=4674)

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        ordering = ['nm_uf']

    def __str__(self):
        return f"Estado - {self.nm_uf}"


class RegiaoIntermediaria(gismodels.Model):
    cd_rgint = gismodels.IntegerField(verbose_name='Código Região Intermediária', primary_key=True, unique=True)
    nm_rgint = gismodels.CharField(verbose_name='Nome Região Intermediária', max_length=200)
    pop2019 = gismodels.IntegerField(verbose_name='População 2019')
    area_km2 = gismodels.DecimalField(verbose_name='Área (km²)', max_digits=20, decimal_places=4)
    contem_mun = gismodels.IntegerField(verbose_name='Qtd de Municípios contidos')
    uf = gismodels.ForeignKey(Estado, on_delete=gismodels.CASCADE, verbose_name='Estado')
    #cd_uf = gismodels.IntegerField(verbose_name='Código UF')
    #nm_uf = gismodels.CharField(verbose_name='Nome do Estado', max_length=100)
    #sigla_uf = gismodels.CharField(verbose_name='Sigla UF', max_length=2)
    #nm_regiao = gismodels.CharField(verbose_name='Nome da Região', max_length=100)
    #long_cent = gismodels.DecimalField(verbose_name='Longitude do Centróide', max_digits=20, decimal_places=8)
    #lat_cent = gismodels.DecimalField(verbose_name='Latitude do Centróide', max_digits=20, decimal_places=8)
    geom = gismodels.MultiPolygonField(verbose_name='Geometria', srid=4674)

    class Meta:
        verbose_name = 'Região Intermediária'
        verbose_name_plural = 'Regiões Intermediárias'
        ordering = ['nm_rgint']

    def __str__(self):
        return f"Região Intermediaria - {self.nm_rgint}"


class RegiaoImediata(gismodels.Model):
    cd_rgi = gismodels.IntegerField(verbose_name='Código Região Imediata', primary_key=True, unique=True)
    nm_rgi = gismodels.CharField(verbose_name='Nome Região Imediata', max_length=200)
    pop2019 = gismodels.IntegerField(verbose_name='População 2019')
    area_km2 = gismodels.DecimalField(verbose_name='Área (km²)', max_digits=20, decimal_places=4)
    contem_mun = gismodels.IntegerField(verbose_name='Qtd de Municípios contidos')
    rgint = gismodels.ForeignKey(RegiaoIntermediaria, on_delete=gismodels.CASCADE, verbose_name='Região Intermediária')
    uf = gismodels.ForeignKey(Estado, on_delete=gismodels.CASCADE, verbose_name='Estado')
    #cd_rgint = gismodels.IntegerField(verbose_name='Código Região Intermediária')
    #nm_rgint = gismodels.CharField(verbose_name='Nome Região Intermediária', max_length=200)
    #cd_uf = gismodels.IntegerField(verbose_name='Código UF')
    #nm_uf = gismodels.CharField(verbose_name='Nome do Estado', max_length=100)
    #sigla_uf = gismodels.CharField(verbose_name='Sigla UF', max_length=2)
    #nm_regiao = gismodels.CharField(verbose_name='Nome da Região', max_length=100)
    #long_cent = gismodels.DecimalField(verbose_name='Longitude do Centróide', max_digits=20, decimal_places=8)
    #lat_cent = gismodels.DecimalField(verbose_name='Latitude do Centróide', max_digits=20, decimal_places=8)
    geom = gismodels.MultiPolygonField(verbose_name='Geometria', srid=4674)

    class Meta:
        verbose_name = 'Região Imediata'
        verbose_name_plural = 'Regiões Imediatas'
        ordering = ['nm_rgi']

    def __str__(self):
        return f"Região Imediata - {self.nm_rgi}"


class Municipio(gismodels.Model):
    cd_mun = gismodels.IntegerField(verbose_name='Código Município', primary_key=True, unique=True)
    nm_mun = gismodels.CharField(verbose_name='Nome Município', max_length=200)
    pop2019 = gismodels.IntegerField(verbose_name='População 2019')
    cep = gismodels.IntegerField(verbose_name='CEP')
    area_km2 = gismodels.DecimalField(verbose_name='Área (km²)', max_digits=20, decimal_places=4)
    rgi = gismodels.ForeignKey(RegiaoImediata, on_delete=gismodels.CASCADE, verbose_name='Região Imediata')
    rgint = gismodels.ForeignKey(RegiaoIntermediaria, on_delete=gismodels.CASCADE, verbose_name='Região Intermediária')
    uf = gismodels.ForeignKey(Estado, on_delete=gismodels.CASCADE, verbose_name='Estado')
    #cd_rgi = gismodels.IntegerField(verbose_name='Código Região Imediata')
    #nm_rgi = gismodels.CharField(verbose_name='Nome Região Imediata', max_length=200)
    #cd_rgint = gismodels.IntegerField(verbose_name='Código Região Intermediária')
    #nm_rgint = gismodels.CharField(verbose_name='Nome Região Intermediária', max_length=200)
    #cd_uf = gismodels.IntegerField(verbose_name='Código UF')
    #nm_uf = gismodels.CharField(verbose_name='Nome do Estado', max_length=100)
    #sigla_uf = gismodels.CharField(verbose_name='Sigla UF', max_length=2)
    #nm_regiao = gismodels.CharField(verbose_name='Nome da Região', max_length=100)
    #long_cent = gismodels.DecimalField(verbose_name='Longitude do Centróide', max_digits=20, decimal_places=8)
    #lat_cent = gismodels.DecimalField(verbose_name='Latitude do Centróide', max_digits=20, decimal_places=8)
    geom = gismodels.MultiPolygonField(verbose_name='Geometria', srid=4674)

    class Meta:
        verbose_name = 'Município'
        verbose_name_plural = 'Municípios'
        ordering = ['nm_mun']
    
    def __str__(self):
        return f"Município - {self.nm_mun}"
