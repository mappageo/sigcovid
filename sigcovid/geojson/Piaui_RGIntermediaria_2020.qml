<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" readOnly="0" labelsEnabled="1" simplifyDrawingHints="1" simplifyLocal="1" minScale="100000000" maxScale="0" simplifyAlgorithm="0" version="3.14.0-Pi" styleCategories="AllStyleCategories" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal enabled="0" endField="" durationField="" startExpression="" mode="0" fixedDuration="0" startField="" endExpression="" durationUnit="min" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="singleSymbol" forceraster="0" enableorderby="0">
    <symbols>
      <symbol clip_to_extent="1" name="0" force_rhr="0" alpha="1" type="fill">
        <layer enabled="1" class="SimpleFill" locked="0" pass="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="231,113,72,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="132,0,255,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="no" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style blendMode="0" fontSize="12" fontLetterSpacing="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" multilineHeight="1" textOrientation="horizontal" fontStrikeout="0" isExpression="0" textColor="0,0,0,255" fontWeight="63" textOpacity="1" fieldName="NM_RGINT" previewBkgrdColor="255,255,255,255" fontKerning="1" fontSizeUnit="Pixel" fontCapitals="0" fontWordSpacing="0" fontFamily="Open Sans" fontItalic="0" fontUnderline="0" useSubstitutions="0" allowHtml="0" namedStyle="Semibold">
        <text-buffer bufferSize="1" bufferBlendMode="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="1" bufferOpacity="1" bufferJoinStyle="128" bufferSizeUnits="MM" bufferColor="255,255,255,255" bufferDraw="0"/>
        <text-mask maskEnabled="0" maskJoinStyle="128" maskOpacity="1" maskSizeUnits="MM" maskedSymbolLayers="" maskType="0" maskSize="1.5" maskSizeMapUnitScale="3x:0,0,0,0,0,0"/>
        <background shapeOffsetUnit="MM" shapeSizeY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBorderColor="128,128,128,255" shapeRotationType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeSVGFile="" shapeFillColor="255,255,255,255" shapeBlendMode="0" shapeType="0" shapeJoinStyle="64" shapeBorderWidthUnit="MM" shapeRadiiX="0" shapeRadiiUnit="MM" shapeDraw="0" shapeOpacity="1" shapeBorderWidth="0" shapeSizeX="0" shapeOffsetY="0" shapeSizeUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetX="0" shapeRotation="0" shapeSizeType="0">
          <symbol clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1" type="marker">
            <layer enabled="1" class="SimpleMarker" locked="0" pass="0">
              <prop v="0" k="angle"/>
              <prop v="255,158,23,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString"/>
                  <Option name="properties"/>
                  <Option name="type" value="collection" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetAngle="135" shadowOpacity="0.7" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowScale="100" shadowColor="0,0,0,255" shadowOffsetUnit="MM" shadowOffsetDist="1" shadowRadiusUnit="MM" shadowOffsetGlobal="1" shadowRadius="1.5" shadowBlendMode="6" shadowDraw="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format autoWrapLength="0" addDirectionSymbol="0" reverseDirectionSymbol="0" rightDirectionSymbol=">" placeDirectionSymbol="0" formatNumbers="0" plussign="0" decimals="3" leftDirectionSymbol="&lt;" useMaxLineLengthForAutoWrap="1" multilineAlign="1" wrapChar=" "/>
      <placement quadOffset="4" priority="5" distUnits="MM" yOffset="0" preserveRotation="1" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" polygonPlacementFlags="2" maxCurvedCharAngleOut="-25" placementFlags="10" centroidInside="0" geometryGeneratorType="PointGeometry" fitInPolygonOnly="0" placement="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" layerType="PolygonGeometry" offsetType="0" maxCurvedCharAngleIn="25" overrunDistance="0" distMapUnitScale="3x:0,0,0,0,0,0" rotationAngle="0" centroidWhole="0" geometryGenerator="" repeatDistance="0" xOffset="0" geometryGeneratorEnabled="0" dist="0" repeatDistanceUnits="MM" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetUnits="MM" overrunDistanceUnit="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0"/>
      <rendering zIndex="0" labelPerPart="0" fontLimitPixelSize="0" displayAll="0" scaleVisibility="0" upsidedownLabels="0" fontMaxPixelSize="10000" scaleMax="0" obstacle="1" scaleMin="0" obstacleFactor="1" drawLabels="1" maxNumLabels="2000" obstacleType="1" mergeLines="0" minFeatureSize="0" fontMinPixelSize="3" limitNumLabels="0"/>
      <dd_properties>
        <Option type="Map">
          <Option name="name" value="" type="QString"/>
          <Option name="properties" type="Map">
            <Option name="PositionX" type="Map">
              <Option name="active" value="true" type="bool"/>
              <Option name="field" value="auxiliary_storage_labeling_positionx" type="QString"/>
              <Option name="type" value="2" type="int"/>
            </Option>
            <Option name="PositionY" type="Map">
              <Option name="active" value="true" type="bool"/>
              <Option name="field" value="auxiliary_storage_labeling_positiony" type="QString"/>
              <Option name="type" value="2" type="int"/>
            </Option>
          </Option>
          <Option name="type" value="collection" type="QString"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" value="pole_of_inaccessibility" type="QString"/>
          <Option name="ddProperties" type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
          <Option name="drawToAllParts" value="false" type="bool"/>
          <Option name="enabled" value="1" type="QString"/>
          <Option name="labelAnchorPoint" value="point_on_exterior" type="QString"/>
          <Option name="lineSymbol" value="&lt;symbol clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot; type=&quot;line&quot;>&lt;layer enabled=&quot;1&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot; pass=&quot;0&quot;>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString"/>
          <Option name="minLength" value="0" type="double"/>
          <Option name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="minLengthUnit" value="MM" type="QString"/>
          <Option name="offsetFromAnchor" value="0" type="double"/>
          <Option name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="offsetFromAnchorUnit" value="MM" type="QString"/>
          <Option name="offsetFromLabel" value="0" type="double"/>
          <Option name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="offsetFromLabelUnit" value="MM" type="QString"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties/>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks type="StringList">
      <Option value="" type="QString"/>
    </activeChecks>
    <checkConfiguration/>
  </geometryOptions>
  <referencedLayers/>
  <referencingLayers/>
  <fieldConfiguration>
    <field name="CD_RGINT">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="NM_RGINT">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="POP2019">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="AREA_KM2">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="CONTEM_MUN">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="CD_UF">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="NM_UF">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SIGLA_UF">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="NM_REGIAO">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LONG_CENT">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LAT_CENT">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="auxiliary_storage_labeling_positionx">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="auxiliary_storage_labeling_positiony">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="CD_RGINT" index="0"/>
    <alias name="" field="NM_RGINT" index="1"/>
    <alias name="" field="POP2019" index="2"/>
    <alias name="" field="AREA_KM2" index="3"/>
    <alias name="" field="CONTEM_MUN" index="4"/>
    <alias name="" field="CD_UF" index="5"/>
    <alias name="" field="NM_UF" index="6"/>
    <alias name="" field="SIGLA_UF" index="7"/>
    <alias name="" field="NM_REGIAO" index="8"/>
    <alias name="" field="LONG_CENT" index="9"/>
    <alias name="" field="LAT_CENT" index="10"/>
    <alias name="" field="auxiliary_storage_labeling_positionx" index="11"/>
    <alias name="" field="auxiliary_storage_labeling_positiony" index="12"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="CD_RGINT" applyOnUpdate="0" expression=""/>
    <default field="NM_RGINT" applyOnUpdate="0" expression=""/>
    <default field="POP2019" applyOnUpdate="0" expression=""/>
    <default field="AREA_KM2" applyOnUpdate="0" expression=""/>
    <default field="CONTEM_MUN" applyOnUpdate="0" expression=""/>
    <default field="CD_UF" applyOnUpdate="0" expression=""/>
    <default field="NM_UF" applyOnUpdate="0" expression=""/>
    <default field="SIGLA_UF" applyOnUpdate="0" expression=""/>
    <default field="NM_REGIAO" applyOnUpdate="0" expression=""/>
    <default field="LONG_CENT" applyOnUpdate="0" expression=""/>
    <default field="LAT_CENT" applyOnUpdate="0" expression=""/>
    <default field="auxiliary_storage_labeling_positionx" applyOnUpdate="0" expression=""/>
    <default field="auxiliary_storage_labeling_positiony" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="0" unique_strength="0" field="CD_RGINT" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="NM_RGINT" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="POP2019" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="AREA_KM2" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="CONTEM_MUN" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="CD_UF" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="NM_UF" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="SIGLA_UF" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="NM_REGIAO" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="LONG_CENT" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="LAT_CENT" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="auxiliary_storage_labeling_positionx" notnull_strength="0" exp_strength="0"/>
    <constraint constraints="0" unique_strength="0" field="auxiliary_storage_labeling_positiony" notnull_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="CD_RGINT" desc=""/>
    <constraint exp="" field="NM_RGINT" desc=""/>
    <constraint exp="" field="POP2019" desc=""/>
    <constraint exp="" field="AREA_KM2" desc=""/>
    <constraint exp="" field="CONTEM_MUN" desc=""/>
    <constraint exp="" field="CD_UF" desc=""/>
    <constraint exp="" field="NM_UF" desc=""/>
    <constraint exp="" field="SIGLA_UF" desc=""/>
    <constraint exp="" field="NM_REGIAO" desc=""/>
    <constraint exp="" field="LONG_CENT" desc=""/>
    <constraint exp="" field="LAT_CENT" desc=""/>
    <constraint exp="" field="auxiliary_storage_labeling_positionx" desc=""/>
    <constraint exp="" field="auxiliary_storage_labeling_positiony" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="CD_RGINT" hidden="0" type="field" width="-1"/>
      <column name="NM_RGINT" hidden="0" type="field" width="-1"/>
      <column name="POP2019" hidden="0" type="field" width="-1"/>
      <column name="AREA_KM2" hidden="0" type="field" width="-1"/>
      <column name="CONTEM_MUN" hidden="0" type="field" width="-1"/>
      <column name="CD_UF" hidden="0" type="field" width="-1"/>
      <column name="NM_UF" hidden="0" type="field" width="-1"/>
      <column name="SIGLA_UF" hidden="0" type="field" width="-1"/>
      <column name="NM_REGIAO" hidden="0" type="field" width="-1"/>
      <column name="LONG_CENT" hidden="0" type="field" width="-1"/>
      <column name="LAT_CENT" hidden="0" type="field" width="-1"/>
      <column name="auxiliary_storage_labeling_positionx" hidden="1" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column name="auxiliary_storage_labeling_positiony" hidden="1" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable/>
  <labelOnTop/>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression></previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
